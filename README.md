# itbastion-internship

Репозиторий предназначен для выполнения заданий на стажировку в компании "Айти Бастион"

В данной ветке опубликован пробный проект блога на фреймворке `flask`.

## Использование

1. Клонировать репозиторий
2. Открыть терминал в папке проекта
3. Последовательно выполнить в терминале следующие команды

```powershell
python -m venv venv

venv\Scripts\activate.bat # для Windows
source venv/bin/activate # для Linux/MacOS

pip install -r requirements.txt
```

4. Создать файл `.env` с переменными окружения

```ini
FLASK_APP=main
FLASK_ENV=development
```

5. Для запуска проекта

```powershell
flask run
```

6. Для запуска Allure надо скачать его в папку с проектом по [этой ссылке](https://github.com/allure-framework/allure2/releases/tag/2.22.4).

## Работа с `pytest` и `allure-pytest`

После установки указанных модулей проделал следующие шаги:

1. Создадим папку `tests` - в ней будем хранить все тесты;
2. В папке `tests` создадим файл `test_routes.py`, в котором напишем три теста:
   1. Тест на существование главной страницы;
   2. Тест на проверку наличия кнопки "New post" на страницах;
   3. Тест на отсутствие страницы `/about`;
3. Скачаем [последний релиз](https://github.com/allure-framework/allure2/releases/tag/2.22.4) с официального репозитория, для удобства поместим его в папку с проектом;
4. Выполним команду для создания отчета о тестах:

```powershell
pytest --alluredir=\tests_allure .\tests\test_routes.py
```

5. Выполним команду для открытия отчета о тестах:

```powershell
.\allure-2.22.4\bin\allure.bat serve \tests_allure
```

Откроется вкладка в браузере по умолчанию со следующими данными:

![alt](https://sun9-30.userapi.com/impg/ie2G0bssKuiNDpMeApXBMZmtv9YUIstzoGxHSA/7_q49oibaKI.jpg?size=1858x1080&quality=96&sign=ae9f8b81833af93dd80fec359bcfa245&type=album)

![alt2](https://sun9-80.userapi.com/impg/C5TcYDxmHoBSAW8WFlRM-glWReBaoSCG3GHm7w/GvXqTPSke9I.jpg?size=1858x1080&quality=96&sign=062c7303404116bc7b8bd90fb12297dc&type=album)



Лог командной строки
```powershell
(venv) B:\GITLAB\itbastion-intership>pytest --alluredir=\tests_allure .\tests\test_routes.py
============================ test session starts ============================
platform win32 -- Python 3.11.1, pytest-7.4.0, pluggy-1.2.0
rootdir: B:\GITLAB\itbastion-intership
plugins: allure-pytest-2.13.2
collected 3 items

tests\test_routes.py ...                                               [100%]

============================= 3 passed in 0.36s =============================

(venv) B:\GITLAB\itbastion-intership>.\allure-2.22.4\bin\allure.bat serve \tests_allure
Generating report to temp directory...
Report successfully generated to C:\Users\podst\AppData\Local\Temp\8828591508684907270\allure-report
Starting web server...
2023-06-28 11:04:47.332:INFO::main: Logging initialized @3006ms to org.eclipse.jetty.util.log.StdErrLog
Server started at <http://26.137.65.188:56480/>. Press <Ctrl+C> to exit
```




